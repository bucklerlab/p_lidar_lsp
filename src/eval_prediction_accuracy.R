eval_prediction_accuracy <- function(taxa, response, explanatory, fold, ncomp){
  library(pls)
  
  explanatory <- as.matrix(explanatory)
  response <- as.matrix(response)
  
  uniqueTaxa <- unique(taxa)
  randomizedTaxa <- sample(uniqueTaxa) # We can then iterate through this sequentially

  fold_size <- ceiling(length(uniqueTaxa) / fold)
  accuracy <- NULL
  allPred <- NULL
  truth <- NULL
  for(i in 1:fold){
    test_index <- (fold_size*(i-1)) + (1:fold_size)
    
    # Truncate test_index if it will extend beyond the number of taxa
    if(max(test_index) > length(uniqueTaxa)){
      test_index <- min(test_index):length(uniqueTaxa)
    }

    test_taxa <- randomizedTaxa[test_index]
    
    test <- taxa %in% test_taxa
    train <- !test
    
    model <- plsr(response ~ explanatory, ncomp=ncomp, subset=train)
    prediction <- predict(model, newdata=explanatory[test,], type="response")
    accuracy <- rbind(accuracy,
                      diag(cor(response[test,,drop=FALSE], prediction[,,ncomp], use="complete.obs") )
                     )
    truth <- c(truth, response[test, ])
    allPred <- c(allPred, prediction[,,ncomp])
  }
  
  # browser()
  return(accuracy)
}
