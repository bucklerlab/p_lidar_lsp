convert_to_days <- function(flwDates, plantingDates, format){
  flwDates <- as.Date(flwDates, format=format)
  plantingDates <- as.Date(plantingDates, format=format)
  DTFlw <- as.vector(flwDates - plantingDates)
  print(range(DTFlw, na.rm=TRUE))
  return(DTFlw)
}