fileIsAbsentOrEmpty <- function(path){
  ## Tells whether a file either doesn't exist or is empty
  ## Input: path to the file in question
  ## Output: TRUE/FALSE indicating whether the file is empty or doesn't exist.
  
  exists <- file.exists(path)
  empty  <- file.info(path)$size == 0
  
  return( (!exists) | empty)
}

anyMissingOrEmptyFiles <- function(transitionFramesFile, cameraLogFile, lidarFile){
  ## Checks whether any of the files necessary for splitting up lidar from a
  ##  rover data collection are missing.
  ## Inputs:
  ##    transitionFramesFile: path to file containing the frame numbers
  ##                          that correspond to plot/alley transitions
  ##    cameraLogFile:        path to the camera log file
  ##    lidarFile:            path to the lidar data file
  ## Output: TRUE if any of the inputs are missing or empty, false otherwise.

  if(fileIsAbsentOrEmpty(transitionFramesFile)){
    print(paste0("Skipping ", run, ": Transition frames file is bad."))
    return(TRUE)
  }
  if(fileIsAbsentOrEmpty(cameraLogFile)){
    print(paste0("Skipping ", run, ": Camera log file is bad."))
    return(TRUE)
  }
  if(fileIsAbsentOrEmpty(lidarFile)){
    print(paste0("Skipping ", run, ": LiDar file is bad."))
    return(TRUE)
  }
  return(FALSE)
}

getTransitionTimes <- function(transitionFramesFile, cameraLogFile){
  ## Pulls time stams for plot start/end points, based on the frame
  ##  numbers of plot start/end points.
  ## Inputs:
  ##   transitionFramesFile: path to file with transition frame numbers
  ##                         in a single column
  ##   cameraLogFile:        path to file with camera log, which can be
  ##                         used to link frame numbers to time stamps.
  ## Output: 
  ##   transitionTimes: data.frame with columns for Range, Plot Start, 
  ##                    Plot End, and Run name.
  
  runName <- gsub(".*/(collection.*)_cam.*breakpoints.*", "\\1", transitionFramesFile)
  transitions <- read.table(transitionFramesFile)[,1]
  
  # Transitions are 0-based.  Convert to 1-based:
  transitions <- transitions+1
  
  camLog <- read.csv(cameraLogFile, header=FALSE, skip=1)
  
  transitionTimes <- camLog[transitions, 1]
  transitionTimes <- matrix(transitionTimes, 
                            ncol=2, 
                            byrow=TRUE, 
                            dimnames=list(NULL, c("Start", "Stop")))
  transitionTimes <- data.frame(Range=1:nrow(transitionTimes),
                                transitionTimes, 
                                Run=runName)
  
  return(transitionTimes)
}

isPlot <- function(lidar, transitionTimes){
  ## Labels each time point in a lidar dataset as TRUE/FALSE as to whether
  ##  it is part of a plot or not, and gives the number of the range that 
  ##  the plot timepoints belong to.
  ## Inputs: 
  ##    lidar:           data.frame with columns X, Y, Dist, and Time, 
  ##                     as produced by import_format_lidar_data()
  ##    transitionTimes: 2 column matrix with time stamps of plot start and 
  ##                     end, 1 plot per row.
  ## 
  ## Output: data.frame of the same dimension as lidar with columns for
  ##         isPlot(T/F) and Range number.

  plotRange <- IRanges::IRanges(transitionTimes$Start, transitionTimes$Stop)
  lidarRange <- IRanges::IRanges(lidar$Time, lidar$Time)
  
  whichPlot <- IRanges::findOverlaps(lidarRange, plotRange, select="first")
  
  return(data.frame(isPlot=!is.na(whichPlot),
                    Range=whichPlot))
}


assignLidarFieldInfo <- function(lidar, transitionTimes, column, directionOfTravel){
  ## Assigns column and range numbers to lidar data
  ## Inputs:
  ##  lidar:             data.frame with columns X, Y, Dist, and Time, 
  ##                     as produced by import_format_lidar_data()
  ##  transitionTimes:   2 column matrix with time stamps of plot start and 
  ##                     end, 1 plot per row.
  ##  column:            scalar number of the field column for this run
  ##  directionOfTravel: either "NS" or "SN"
  ##
  ## Output: data.frame same as lidar with additional columns for column,
  ##         range, isPlot(T/F), and direction of travel.
  
  plotInfo <- isPlot(lidar, transitionTimes)
  lidar <- data.frame(lidar, plotInfo)
  
  if(directionOfTravel == "NS"){ # Reverse plot numbers if driven N to S
    maxPlot <- max(lidar$Range, na.rm=TRUE) + 1
    lidar$Range <- maxPlot - lidar$Range
  }
  
  lidar$Column <- column
  lidar$Direction <- directionOfTravel
  
  return(lidar[lidar$isPlot == TRUE, ])
}

getPlotElapsedTime <- function(transitionTimes){
  ## Only used during preliminary data analysis to generate a distribution of
  ## the time it takes to drive a plot.  That distribution will be used to 
  ## discard plots where the rover gets stuck.
  
  print("Getting time distribution...")
  
  elapsedTime <- transitionTimes$Stop - transitionTimes$Start
  times <- data.frame(TimeElapsed=elapsedTime,
                      Range=transitionTimes$Range,
                      Run=transitionTimes$Run)
  return(times)
}

