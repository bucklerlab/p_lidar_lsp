make_phenotypic_model <- function(data, trait, plots=FALSE){
  
   sub <- data %>%
         group_by(PLOT.ID, EXPERIMENT, REP, PEDIGREE, FAMILY, PASS, RANGE) %>%
         select_(trait) %>%
         drop_na() %>%
         summarise_all(mean)
   print(dim(sub))
  
  ## Make model
  if(length(unique(sub$EXPERIMENT))>1){
    form <- reformulate("EXPERIMENT + EXPERIMENT:REP + (1|PEDIGREE)", response=trait)
  } else{
    form <- reformulate("EXPERIMENT:REP + (1|PEDIGREE)", response=trait)
  }
  model <- lmer(form, data=sub, REML=TRUE)
  
  if(plots){  
    ## Save diagnostic figures
    outPath <- paste0("../figs/model_diagnostics/", trait)
    system(paste0("mkdir -p ", outPath))
    
    # Fit vs Resid
    FitVsResid <- data.frame(Fitted=c(fitted(model)),
                             Residuals=c(resid(model)),
                             Family=sub$FAMILY)
    fit_vs_resid <- ggplot(FitVsResid, aes(Fitted, Residuals, col=Family)) +
      geom_point()+
      theme_classic()
    ggsave(paste0(outPath, "/", trait, "_fit_vs_resid.png"),
           fit_vs_resid,
           width=8, height=5)
    
    # QQ plot
    png(paste0(outPath, "/", trait, "_QQ.png"), width=5, height=5, units="in", res=100)
    qqnorm(scale(resid(model))); abline(0,1, col="red")
    dev.off()
    
    # Spatial BLUPs and Residuals
    blups <- scale(ranef(model)$PEDIGREE$"(Intercept)")[match(sub$PEDIGREE, rownames(ranef(model)$PEDIGREE))]
    SpatialResults <- data.frame(rbind(sub[,c("PASS", "RANGE")],sub[,c("PASS", "RANGE")]),
                                 Value=c(blups, scale(resid(model))),
                                 Statistic=rep(c("BLUPs", "Residuals"), each=nrow(sub)))
    spatial_plot <- ggplot(SpatialResults, aes(RANGE, PASS, fill=Value)) +
      geom_raster()+
      facet_grid(~Statistic) +
      theme_classic()
    ggsave(paste0(outPath, "/", trait, "_spatial.png"),
           spatial_plot,
           width=10, height=5)
  }
  
  return(model)
}

estimate_heritability <- function(model, data){
  # From Holland et al., 2003 ESTIMATING AND INTERPRETING HERITABILITY FOR PLANT BREEDING, pg 63-65
  # Uses harmonic mean of plots per genotype to calculate the error variance divisor when experiment
  #  is unbalanced.
  trait <- colnames(model@frame)[1]
  data <- data %>%
          group_by(PLOT.ID, EXPERIMENT, REP, PEDIGREE, FAMILY) %>%
          select_(trait) %>%
          drop_na() %>%
          summarise_all(mean)
  
  # Calculate harmonic mean of plots per genotype:

  f <- length(unique(data$PEDIGREE))
  plots_per_genotype <- table(data$PEDIGREE[!duplicated(data$PLOT.ID)])
  sumPj <- sum(1 / plots_per_genotype)
  ph <- f / sumPj 
  
  # Get variance components:
  components <- data.frame(VarCorr(model), row.names = 1)
  g <- components["PEDIGREE", "vcov"]
  e <- components["Residual", "vcov"]
  
  H2 <- g / (g + e / ph)
  return(H2)
}
