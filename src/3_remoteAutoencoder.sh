#!/bin/bash

rm ../logs/autoencoder_latest.log
ssh redbarn "tensorboard --logdir=/home/labuser/jlg374/lidar_lsp/logs/tensorboardlogs/ --port 6006 &"

nohup ssh redbarn /home/labuser/jlg374/lidar_lsp/src/run_autoencoder_docker_GPU.sh &> ../logs/autoencoder_latest.log &

ssh redbarn -L 6006:132.236.170.200:6006

# ssh redbarn 'kill $!'
ssh redbarn 'docker stop $(docker ps -q)'

