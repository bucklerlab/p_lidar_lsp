#!/bin/sh

cd /lidar_lsp/src

PYTHONHASHSEED=0 python3 lidar_autoencoder_forRemote.py 


# kills the tensorboard process in background
kill $!
