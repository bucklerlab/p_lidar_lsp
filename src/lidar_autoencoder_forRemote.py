seedVal=175

from numpy.random import seed
seed(seedVal)
from tensorflow import set_random_seed
set_random_seed(seedVal)
import random as rn
rn.seed(seedVal)

def read_format_density_map(file_base, nrow, ncol):
    import numpy as np
    # import rpy2.robjects as robj
    # from rpy2.robjects import pandas2ri
    import pandas

    density_file = file_base + ".txt"
    info_file    = file_base + "_info.txt"

    Z = np.fromfile(density_file, sep=" ")
    n_obs = Z.shape[0] / (nrow*ncol)
    img_stack = np.reshape(Z, (int(n_obs), nrow, ncol))

    img_info = pandas.read_table(info_file, sep=" ")

    # Remove border plots (ranges 1 and 34)
    # no_borders = img_info.loc[~img_info['Range'].isin((1,34))]
    # img_stack_no_borders = img_stack[~img_info['Range'].isin((1,34)), :, :]
    no_borders = img_info
    img_stack_no_borders = img_stack

    return img_stack_no_borders, no_borders.reset_index(), img_stack, img_info


def branch_model(input_layer):
    import numpy as np
    from keras.models import Model
    from keras import regularizers
    from keras.layers import Activation, Input, Dense, Flatten, BatchNormalization, Conv2D, AveragePooling2D
    from keras.optimizers import Adagrad

    # in_shape = data[0,:,:].shape
    l2_reg = 0.01
    #
    # branch_in = Input(shape=in_shape)
    branch = Conv2D(8, (21, 21), padding='same', kernel_regularizer=regularizers.l2(l2_reg))(input_layer)
    branch = Activation('relu')(branch)
    branch = BatchNormalization()(branch)

    branch = AveragePooling2D(4, padding='same')(branch)

    branch = Conv2D(1, (3, 3), padding='same', kernel_regularizer=regularizers.l2(l2_reg))(branch)
    branch = Activation('relu')(branch)
    branch = BatchNormalization()(branch)

    branch = AveragePooling2D(2, padding='same')(branch)

    branch = Flatten()(branch)
    branch = Dense(128, activation='relu')(branch)
    branch = Dense(16, activity_regularizer=regularizers.l2(l2_reg))(branch)

    branch_out = Model(input_layer, branch)
    # branch_out.compile(loss='mse', optimizer=Adagrad())
    return branch_out

def make_merger_model(X1, X2):
    import numpy as np
    from keras import regularizers
    from keras.layers import Activation, Average, Reshape, Input, Dense, BatchNormalization, Conv2D, UpSampling2D
    from keras.models import Model
    from keras.optimizers import Adagrad

    l2_reg = 0.01

    branch1_in = Input(X1[0,:,:].shape)
    branch2_in = Input(X2[0,:,:].shape)
    branch1 = branch_model(branch1_in)
    branch2 = branch_model(branch2_in)

    encoded = Average()([branch1.output, branch2.output])
    x = Dense(128, activation='relu')(encoded)
    x = Dense(128, activation='relu')(x)

    x = Reshape((16, 8, 1))(x)  # 32 x 16
    x = UpSampling2D(2)(x)  # 32 x 16
    x = Conv2D(1, (3, 3), padding='same',
               kernel_regularizer=regularizers.l2(l2_reg))(x)  # 128 x 64
    x = Activation('relu')(x)
    x = BatchNormalization()(x)

    x = UpSampling2D(4)(x)  # 128 x 64

    x = Conv2D(8, (21, 21), padding='same',
               kernel_regularizer=regularizers.l2(l2_reg))(x)
    x = Activation('relu')(x)
    x = BatchNormalization()(x)
    # Use a linear activation for the final layer because the
    #   density maps were not bound in [-1, 1]
    decoded = Conv2D(1, (3, 3), padding='same')(x)

    autoencoder = Model([branch1_in, branch2_in], decoded)
    autoencoder.compile(optimizer=Adagrad(), loss='mse')

    encoder = Model([branch1_in, branch2_in], encoded)

    print(autoencoder.summary())

    return autoencoder, encoder


def shuffle_and_split(data, info, max_range, validation_proportion):
    import numpy as np
    from numpy.random import shuffle
    import pandas as pd

    # Adjust Range numbers for bootstrapped plots to reflect
    # their true (original) plot numbers (b/w 1 and 34)
    info["Range"] = info["Range"] % max_range
    info["Range" == 0, "Range"] = max_range
    # Make unique 'plot' identifier
    info["plot"] = "c" + info["Column"].map(str) + "r" + info["Range"].map(str)

    # Split plots randomly into test and train sets

    randomize = pd.unique(info["plot"])
    shuffle(randomize)

    n_val = np.floor(randomize.shape[0] * validation_proportion)
    plot_val = randomize[:int(n_val)]
    plot_train = randomize[int(n_val):]

    in_val   = info["plot"].isin(plot_val)
    in_train = info["plot"].isin(plot_train)

    val = data[in_val, :, :]
    val = np.expand_dims(val, 4)
    train = data[in_train, :, :]
    train = np.expand_dims(train, 4)

    return train, val, in_train, in_val


def standardize_pixel_values(train, mu=None, sd=None):
    import numpy as np
    import scipy.stats as ss
    # import matplotlib.pyplot as plt

    # This needs to be vetted:
    #   Do I have the dimensions correctly specified?
    #   What happens if any of the std == 0 ?

    if mu is None and sd is None:
        mu = np.mean(train, 0)
        sd = np.std(train, 0, ddof=1)
        train_Z = ss.zscore(train, axis=0, ddof=1)
        nans = np.where(np.isnan(train_Z))
        train_Z[nans] = 0

    else:
        train_Z = (train - mu) / sd
        nans = np.where(np.isnan(train_Z))
        train_Z[nans] = 0

    # Reset very high (low) Z values to 6 (-6)
    high = np.where(train_Z > 4)
    low = np.where(train_Z < -4)
    train_Z[high] = 4
    train_Z[low] = -4

    # mx = np.max(train_Z)
    # mn = np.min(train_Z)
    # train_Z = (train_Z - mn) / (mx-mn)

    return train_Z, mu, sd


def make_callbacks():
    from keras.callbacks import TensorBoard, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
    from datetime import datetime

    now = datetime.now()
    now_str = now.strftime("%Y-%m-%d_%H:%M")

    board = TensorBoard(log_dir='../logs/tensorboardlogs/' + now_str,
                        write_images=True,
                        write_grads=True,
                        histogram_freq=1)
    early_stop = EarlyStopping(monitor='val_loss',
                               patience=15,
                               verbose=1)
    checkpoint = ModelCheckpoint('densityImg_autoencoder_ck.h5',
                                 monitor='val_loss',
                                 save_best_only=True)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss',
                                  factor=0.75,
                                  patience=2,
                                  verbose=1)

    callbacks = [board, early_stop, checkpoint]

    return callbacks


def plot_examples(orig, output, imgs, out_path):
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    plt.ioff()

    plt.set_cmap("viridis")

    n_img = imgs.__len__()
    fig = plt.figure(figsize=(3*n_img, 7))
    columns = n_img
    rows = 2
    for i in range(1, n_img+1):
        img_to_plot = imgs[i-1]
        fig.add_subplot(rows, columns, i)
        plt.imshow(orig[img_to_plot,:,:,0])
        fig.add_subplot(rows, columns, i + n_img)
        plt.imshow(output[img_to_plot,:,:,0])
    #plt.tight_layout()
    plt.savefig(out_path, bbox_inches='tight')



def random_positive_or_negative():
    return 1 if np.random.random() < 0.5 else -1


def rotate_images(original, n_repeats, range_min, range_max):

    repeated = np.repeat(original, n_repeats, axis=0)
    rotated = np.zeros(repeated.shape)
    print("Rotating images...")
    for i in range(rotated.shape[0]):
        rot = (range_min - range_max) * np.random.random() + range_max
        direction = random_positive_or_negative()
        rotated[i, :, :, :] = interp.rotate(repeated[i, :, :, :], angle=rot * direction, reshape=False)
    return rotated


def add_noise(original, sd_scale=0.1):
    mu = np.mean(original)
    sd = np.std(original)
    noise = np.random.randn(original.shape[0],
                            original.shape[1],
                            original.shape[2],
                            original.shape[3])
    noise = ((sd * noise) + mu) * sd_scale
    return original + noise

def plot_augmentation_example(trainX1, trainX2, img_num,
                              n_replications, min_rotation, max_rotation, augmentation_noise,
                              out_path=None):
    # original1 and original2 should be two images, rep1 and rep2 of a plot
    import matplotlib
    import matplotlib.pyplot as plt

    if out_path is not None:
        matplotlib.use('Agg')
        plt.ioff()

    np.random.seed(345)

    plt.set_cmap("viridis")

    S1, _, _ = standardize_pixel_values(trainX1)
    S2, _, _ = standardize_pixel_values(trainX2)

    original1 = S1[np.newaxis, img_num, :, :]
    original2 = S2[np.newaxis, img_num, :, :]

    rot1 = rotate_images(original1, n_replications, min_rotation, max_rotation)
    rot2 = rotate_images(original2, n_replications, min_rotation, max_rotation)
    noise1 = add_noise(rot1, augmentation_noise)
    noise2 = add_noise(rot2, augmentation_noise)

    fig = plt.figure(figsize=(22.75, 9))
    columns = 13
    rows = 3

    enc = encoder.predict([noise1, noise2])
    dec = autoencoder.predict([noise1, noise2])

    enc_orig1 = encoder.predict([original1, original1])
    enc_orig2 = encoder.predict([original2, original2])

    dec_orig1 = autoencoder.predict([original1, original1])
    dec_orig2 = autoencoder.predict([original2, original2])

    # Plot originals
    fig.add_subplot(rows, columns, 14)
    plt.imshow(original1[0,:,:,0], origin="lower", interpolation="none")
    plt.axis('off')
    fig.add_subplot(rows, columns, 15)
    plt.imshow(original2[0,:,:,0], origin="lower", interpolation="none")
    plt.axis('off')
    # Plot encoded originals
    # fig.add_subplot(rows, columns, 50)
    # plt.imshow(enc_orig1[0, :, np.newaxis], origin="lower", interpolation="none")
    # plt.axis("off")
    # fig.add_subplot(rows, columns, 63)
    # plt.imshow(enc_orig2[0, :, np.newaxis], origin="lower", interpolation="none")
    # plt.axis("off")
    # # Plot decoded originals
    # fig.add_subplot(rows, columns, 52)
    # plt.imshow(dec_orig1[0, :, :, 0], origin="lower", interpolation="none")
    # plt.axis("off")
    # fig.add_subplot(rows, columns, 65)
    # plt.imshow(dec_orig2[0, :, :, 0], origin="lower", interpolation="none")
    # plt.axis("off")

    for i in (0,2):
        # Plot rotations
        fig.add_subplot(rows, columns, (i*columns + 5))
        plt.imshow(rot1[i,:,:,0], origin="lower", interpolation="none")
        plt.axis('off')
        fig.add_subplot(rows, columns, (i*columns + 6))
        plt.imshow(rot2[i,:,:,0], origin="lower", interpolation="none")
        plt.axis('off')

        # Plot noise
        fig.add_subplot(rows, columns, (i*columns + 8))
        plt.imshow(noise1[i,:,:,0], origin="lower", interpolation="none")
        plt.axis('off')
        fig.add_subplot(rows, columns, (i*columns + 9))
        plt.imshow(noise2[i,:,:,0], origin="lower", interpolation="none")
        plt.axis('off')

        # Plot encoded
        fig.add_subplot(rows, columns, (i*columns + 11))
        plt.imshow(enc[i,:,np.newaxis], origin="lower", interpolation="none")
        plt.axis('off')
        # Plot decoded
        fig.add_subplot(rows, columns, (i*columns + 13))
        plt.imshow(dec[i,:,:,0], origin="lower", interpolation="none")
        plt.axis('off')

    fig.subplots_adjust(wspace=0.0, hspace=0.0)
    fig.tight_layout()

    if out_path is not None:
        fig.savefig(out_path, bbox_inches='tight', transparent=True)


######################################################
if __name__ == '__main__':
    import numpy as np
    import scipy.ndimage.interpolation as interp
    from keras.preprocessing.image import ImageDataGenerator
    from PIL import Image
    import re

    file_base = "../data/G2F_NYH2_densities_64_by_128_clean"

    # Retrain the model? Use false if generating figs, etc.
    retrain = True

    # Image size info
    nrow=128
    ncol=64
    max_range = 34  # This shouldn't matter anymore (defunct)

    # Model fitting info
    val_proportion = 0.2      # proportion of data used for validation
    augmentation_noise = 0.1  # Noise variance to add for augmentation
    n_replications = 5  # Number of times each image replicated for augmentation
    min_rotation = 0    # minimum degrees of rotation for replicated images
    max_rotation = 10   # maximum degrees of rotation for replicated images

    D, info, D_all, info_all = read_format_density_map(file_base, nrow, ncol)

    # Find first and last occurrences of any plots that occur more
    #  than once
    r1 = info.duplicated(subset=["Range", "Column"], keep=False) &\
        ~info.duplicated(subset=["Range", "Column"], keep='first')
    r2 = info.duplicated(subset=["Range", "Column"], keep=False) &\
        ~info.duplicated(subset=["Range", "Column"], keep='last')

    info_r1 = info.loc[r1].sort_values(["Range", "Column"])
    info_r2 = info.loc[r2].sort_values(["Range", "Column"])

    D_r1 = D[info_r1.index, :, :]
    D_r2 = D[info_r2.index, :, :]
    X_r1r2 = np.concatenate((D_r1, D_r2), 0)
    infoX_r1r2 = info_r1.append(info_r2)
    X_r2r1 = np.concatenate((D_r2, D_r1), 0)
    Y_r1r2 = np.concatenate((D_r1, D_r2), 0)

    trainX1, valX1, which_train, which_val = shuffle_and_split(X_r1r2, infoX_r1r2, max_range, val_proportion)
    trainX2 = np.expand_dims(X_r2r1[which_train, :, :], 3)
    valX2   = np.expand_dims(X_r2r1[which_val,   :, :], 3)
    trainY  = np.expand_dims(Y_r1r2[which_train, :, :], 3)
    valY    = np.expand_dims(Y_r1r2[which_val,   :, :], 3)

    # Set up tensorboard, early stopping and model checkpoints
    callbacks = make_callbacks()

    # Preprocess images by replicating each one 5 times,
    # randomly rotating each image, standardizing pixel
    # values, and adding random noise.
    trainX1_rot = rotate_images(trainX1, n_replications, min_rotation, max_rotation)
    valX1_rot   = rotate_images(valX1,   n_replications, min_rotation, max_rotation)
    trainX2_rot = rotate_images(trainX2, n_replications, min_rotation, max_rotation)
    valX2_rot   = rotate_images(valX2,   n_replications, min_rotation, max_rotation)

    trainX1_rot, mu, sd = standardize_pixel_values(trainX1_rot)
    valX1_rot, _, _     = standardize_pixel_values(valX1_rot, mu, sd)
    trainX2_rot, mu, sd = standardize_pixel_values(trainX2_rot)
    valX2_rot, _, _     = standardize_pixel_values(valX2_rot, mu, sd)

    trainX1_rot = add_noise(trainX1_rot, augmentation_noise)
    valX1_rot   = add_noise(valX1_rot,   augmentation_noise)
    trainX2_rot = add_noise(trainX2_rot, augmentation_noise)
    valX2_rot   = add_noise(valX2_rot,   augmentation_noise)

    to_encode, _, _ = standardize_pixel_values(np.expand_dims(D_all, 3), mu, sd)

    X1  = np.repeat(trainX1_rot, 2, axis=0)
    X2  = np.repeat(trainX2_rot, 2, axis=0)
    X1v = np.repeat(valX1_rot, 2, axis=0)
    X2v = np.repeat(valX2_rot, 2, axis=0)

    Y = np.ndarray(X1.shape)
    Y[0::2, ] = trainX1_rot
    Y[1::2, ] = trainX2_rot
    Yv  = np.ndarray(X1v.shape)
    Yv[0::2, ] = valX1_rot
    Yv[1::2, ] = valX2_rot

    autoencoder, encoder = make_merger_model(X1, X2)

    # Fit autoencoder
    if retrain:
        autoencoder.fit([X1, X2], Y,
                        epochs=200,
                        batch_size=256,
                        shuffle=True,
                        validation_data=([X1v, X2v], Yv),
                        callbacks=callbacks)

    # Load weights from the optimal model before early stopping
    autoencoder.load_weights('densityImg_autoencoder_ck.h5')

    # Encode and decode the original data
    encoded_imgs = encoder.predict([to_encode, to_encode])
    decoded_imgs = autoencoder.predict([to_encode, to_encode])

    out_cols = np.prod(encoded_imgs.shape[1:])
    out = np.reshape(encoded_imgs, (D_all.shape[0], out_cols))

    out_file = file_base + "_encoded.txt"
    np.savetxt(out_file, out)

    # Plot examples
    plot_examples(to_encode, decoded_imgs, [0,2,4], "../figs/encoded_examples.png")
    # plot_augmentation_example(trainX1, trainX2, 31)
    whichplot = 30
    plot_augmentation_example(trainX1, trainX2, whichplot,
                              n_replications, min_rotation, max_rotation, augmentation_noise,
                              "../figs/augmentation_example_"+str(whichplot)+".png")