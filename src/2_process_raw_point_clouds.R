setwd("~/projects/lidar_lsp/src/")

library(plyr)
library(dplyr)
library(mmand)

pointCloudDir <- "/media/jlg374/Data1/rover_campaign/point_clouds/"
badPlotsFile  <- "../data/plots_to_remove.txt"
source("utility_functions.R")
source("remove_singleton_voxels.R")

## Threshold (duration) for removing plots
## that took a long time to drive through (crashes, etc):
absThreshold <- 15000
## Number of bins to use for density calculation of plot biomass:
nbin <- c(64, 128)
## Should all density maps be made to cover the same amount of
##  physical space? (e.g., all 128x64 maps correspond to 2 meters 
##  by 1 meter)
standardSize = FALSE
## Should all density maps be normalized to sum(density) = 1?
normalize = FALSE

remove_ground_and_background <- function(pointCloud, kernelSize=65, nPoints=1080){
  # Requires EBImage
  # Format point cloud into distances and threshold to retain only closest 
  #  row of plants.
  # Inputs:
  #   pointCloud: lidar point cloud data, with a column named 'Dist' containing
  #               raw distance values.
  #   kernelSize: Size of square kernel used to morphologically open the lidar
  #               data in order to identify points belonging to the ground.
  #   nPoints:    Number of data points recorded by the LiDar at each timepoint
  # Outputs: a list with two objects:
  #   [[1]]: a point cloud that has been cleaned by removing points belonging
  #          to the ground or adjacent rows of plants. 
  #   [[2]]: the original distance data, formatted as a matrix (similar to
  #          data format originally recorded by the LiDar unit).

  library(EBImage)
   
  raw <- matrix(pointCloud$Dist, nrow=nPoints)
  horizontalDist <- abs(apply(raw, 2, function(x) make_cartesian(x)$X))
  tooFar <- raw == 65533
  raw[tooFar] <- max(raw[!tooFar])
  horizontalDist[tooFar] <- max(horizontalDist[!tooFar])
  thresh <- otsu(log(horizontalDist), range(log(horizontalDist)))
  
  bin <- log(horizontalDist) < thresh
  
  k <- matrix(1, nrow=kernelSize, ncol=kernelSize)
  ground <- EBImage::opening(bin, k)
  
  
  raw[!bin] <- NA
  raw[ground] <- NA

  newPointCloud <- pointCloud
  newPointCloud$Dist <- as.vector(raw)
  newPointCloud <- newPointCloud[!is.na(newPointCloud$Dist), ]
  return(list(newPointCloud, raw))  
}

#######################################################
## MAIN
#######################################################
paths <-  list.files(pointCloudDir, pattern="[S,N]{1}.txt", full.names = TRUE)
allDensity <- matrix()
times <- c()
nTimePoints <- c()
for(pointCloudPath in paths){
  print(pointCloudPath)
  pointCloud <- data.table::fread(pointCloudPath, 
                                  header=TRUE, stringsAsFactors=FALSE, 
                                  sep="\t", data.table=FALSE)
  
  ## Eliminate plots if too long - indicating rover got stuck or had trouble
  timeDiff <- diff(range(pointCloud$Time))
  uniqueTimePoints <- length(unique(pointCloud$Time))
  if(timeDiff > absThreshold){
    print("Skipping - longer than time threshold")
    next
  }
  times <- c(times, timeDiff)
  nTimePoints <- c(nTimePoints, uniqueTimePoints)
  
  # Remove points belonging to the ground or adjacent rows of plants
  cleaned <- remove_ground_and_background(pointCloud)
  cleanDF <- cleaned[[1]]

  # timeMapper keeps track of the timing of each slice, otherwise it
  # gets lost when the data frame is converted to voxels.
  timeMapper <- data.frame(Time=unique(cleanDF$Time),
                           Index=as.numeric(as.factor(unique(cleanDF$Time))))

  # Convert to voxel array, then get rid of any voxels that only have one
  #  data point in them and do not neighbor any other voxels with data.
  print("Making voxel array")
  toFlatten <- remove_singleton_voxels(cleanDF, timeMapper)
  
  # Write out cleaned data frame
  outPath <- gsub(".txt", "_clean.txt", pointCloudPath)
  write.table(cleanDF, outPath, 
              row.names=FALSE, col.names=TRUE, 
              sep="\t", quote=FALSE)

  # Bind density into a large matrix with all plots
  density <- flatten(toFlatten, 
                     fold=TRUE, 
                     nbin=nbin, 
                     normalize = normalize, 
                     standardSize = standardSize)
  density$plot <- sub("(.*/)(collection-.*[S,N]{2}).txt", "\\2", pointCloudPath)
  if(pointCloudPath == paths[1]){
   allDensity <- density
  } else{
   allDensity <- bind_rows(allDensity, density)
  }

}

## Write out all densities bound into a single matrix, along with info
Z <- matrix(allDensity$Z, nrow=length(unique(allDensity$plot)), byrow=TRUE)
info <- strsplit(unique(allDensity$plot), "_")
info <- do.call(rbind, info)
Zinfo <- data.frame(Range=as.numeric(sub("r", "", info[,4])),
                    Column=as.numeric(sub("c", "", info[,3])),
                    Direction=info[,5],
                    Run=paste(info[,1], info[,2], sep="_"),
                    TimeDiff=times,
                    UniqueTimePoints=nTimePoints,
                    stringsAsFactors=FALSE)

# Remove plots that were deemed untrustworthy by visual analysis of images
bad_plots <- read.table(badPlotsFile, header = FALSE, stringsAsFactors = FALSE)[,1]
plot_unique <- paste0(Zinfo$Run,
                      "_c", Zinfo$Column,
                      "_r", Zinfo$Range,
                      "_", Zinfo$Direction,
                      ".png")
to_remove <- plot_unique %in% bad_plots
Zinfo <- Zinfo[!to_remove, ]
Z <- Z[!to_remove, ]

# Write files out
Z_outfile      <- paste0("../data/G2F_NYH2_densities_", nbin[1], "_by_", nbin[2],"_clean.txt")
Zinfo_outfile  <- paste0("../data/G2F_NYH2_densities_", nbin[1], "_by_", nbin[2],"_clean_info.txt")

write.table(Z, Z_outfile, quote=FALSE, row.names=FALSE, col.names=FALSE)
write.table(Zinfo, Zinfo_outfile, quote=FALSE, row.names=FALSE, col.names=TRUE)

