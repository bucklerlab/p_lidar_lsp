
cd /home/labuser/jlg374/labdocker

# Path to the autoencoder run script IN THE DOCKER
RUN_AE=/lidar_lsp/src/start_autoencoder.sh

nvidia-docker run -u root -itd --rm -p 8888:8888 \
    --device=/dev/nvidia0 \
    --device=/dev/nvidiactl \
    --device=/dev/nvidia-modeset \
    --device=/dev/nvidia-uvm \
    --device=/dev/nvidia-uvm-tools \
    $(ls /usr/lib/*-linux-gnu/{libcuda,libnvidia}* | xargs -I{} echo '-v {}:{}:ro') \
    -v /home/labuser/jlg374/lidar_lsp/:/lidar_lsp/:z,shared \
    labdocker

DOCK=$( docker ps -q )

nvidia-docker exec $DOCK $RUN_AE



