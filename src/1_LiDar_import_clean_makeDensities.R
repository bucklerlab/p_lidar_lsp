setwd("~/projects/lidar_lsp/src/")

# Import functions #
####################
source("assignLidarFieldInfo.R")
source("utility_functions.R")

# Set parameters #
##################

## Number of ranges in one column of the field:
nRanges <- 34

# Set file paths #
##################

## This is a manually curated metadata file with information about date, time,
##  and direction of travel for each rover file.
runInfoFile <- "../data/rover_run_metadata_CLEAN.csv"
## Paths to directories with camera logs, raw lidar data, and time breakpoints for
##  segmenting plots:
camLogDirectory <- "/media/jlg374/Data1/rover_campaign/G2F_left_camera_logs/"
lidarDirectory <- "/media/jlg374/Data1/rover_campaign/G2F_vert_lidar/"
breakpointDirectory <- "../data/segmentation_breakpoint_files/"

# Main #
########

breakpointFiles <- list.files(breakpointDirectory)
runNames <- gsub("_cam_left_breakpoints.txt", "", breakpointFiles)

runInfo <- read.csv(runInfoFile, stringsAsFactors = FALSE)
runInfo$runName <- gsub(".zip", "", runInfo$File)
for(run in runNames){
  print(paste0("Starting run ", run, "..."))
  
  ## Generate data file names
  transitionFramesFile <- paste0(breakpointDirectory, run, "_cam_left_breakpoints.txt")
  cameraLogFile        <- paste0(camLogDirectory, run, "_cam_left_log") 
  lidarFile            <- paste0(lidarDirectory, run, "_vert_lidar")
  
  if(anyMissingOrEmptyFiles(transitionFramesFile, cameraLogFile, lidarFile)) next
  
  ## Extract descriptive data about this run from metadata
  whichRun  <- which(runInfo$runName == run)
  field     <- runInfo$Field[whichRun]
  column    <- runInfo$Column[whichRun]
  direction <- runInfo$Direction[whichRun]
  
  ## Convert transition frame numbers to time stamps
  transitionTimes <- getTransitionTimes(transitionFramesFile, cameraLogFile)
  
  ## Skip this run if it doesn't have the correct number of ranges
  if(nrow(transitionTimes) != nRanges){
    print(paste0("Skipping ", run, ": Incorrect number of ranges."))
    next
  }
  
  ## Import lidar and format in cartesian coords
  print("Importing LiDar...")
  lidar <- import_format_lidar_data(lidarFile)
  print("Assigning plot info to LiDar...")
  lidar <- assignLidarFieldInfo(lidar, transitionTimes, column, direction)
  lidar$Run <- run
  
  ## Write point clouds for each plot to separate files
  pointCloudDir <- "/media/jlg374/Data1/rover_campaign/point_clouds/"
  for(range in unique(lidar$Range)){
    thisPlot <- lidar[lidar$Range == range, ]
    thisPlotFile <- paste0(pointCloudDir,
                           thisPlot$Run[1], "_c",
                           thisPlot$Column[1], "_r",
                           thisPlot$Range[1], "_", 
                           thisPlot$Direction[1], ".txt")
    print(paste0("Writing ", thisPlotFile))
    write.table(thisPlot[,c("X","Y","Time","Dist")], thisPlotFile,
                row.names=FALSE, col.names=TRUE, sep="\t", quote=FALSE)
  }
  
  # The distribution of elapsed times was used to determine the value for
  #  the variable 'absThreshold', which is used in 2_process_raw_point_clouds.R
  if(run == runNames[1]){
    # allFlat <- flat
    times <- getPlotElapsedTime(transitionTimes)
  } else{
    # allFlat <- rbind(allFlat, flat)
    times <- rbind(times, getPlotElapsedTime(transitionTimes))
  }
  
}