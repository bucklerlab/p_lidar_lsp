setwd("~/projects/lidar_lsp/src/")

library(tidyverse)
library(magrittr)
library(lme4)
source("model_phenotype_functions.R")

pheno_path <- "../data/NYH2_allTraits_hand_rover.txt"

pheno <- read.table(pheno_path, stringsAsFactors = FALSE, header=TRUE)
trait_names <- colnames(pheno)[19:64]
info_cols <- c("PLOT.ID", "EXPERIMENT", "REP", "PEDIGREE", "FAMILY", "PASS", "RANGE")
pheno <- pheno %>%
  group_by_(.dots=info_cols, include="RANGE") %>%
  select_(.dots=trait_names)

## PCA on manual traits
architecture_traits <- c("PLANT.HEIGHT", "EAR.HEIGHT", "STAND.COUNT", 
                         "ROOT.LODGING", "STALK.LODGING", "TOTAL.LEAVES", 
                         "EAR.LEAF")
# manual_traits <- colnames(pheno)[43:54]
manual_traits <- architecture_traits
manual_pca <- pheno %>%
  summarise_all(mean) %>%
  ungroup() %>%
  select_(.dots=manual_traits) %>%
  drop_na() %>%
  scale() %>%
  prcomp() %$% x
info <- pheno %>%
  summarise_all(mean) %>%
  select_(.dots=manual_traits) %>%
  drop_na() %>%
  select_(.dots=info_cols)
manual_pca <- data.frame(manual_pca, info) %>%
  group_by_(.dots=info_cols, include="RANGE") %>%
  summarise_all(mean)

## PCA on autoencoder LSPs
enc_traits <- grep("ENC", colnames(pheno), value = TRUE)
enc_pca <- pheno %>%
  ungroup() %>% 
  select_(.dots=enc_traits) %>%
  drop_na() %>%
  prcomp(scale.=TRUE) %$% x
info <- pheno %>%  
  select_(.dots=enc_traits) %>%
  drop_na() %>%
  select_(.dots=info_cols)
enc_pca <- data.frame(enc_pca, info)%>%
  group_by_(.dots=info_cols, include="RANGE") %>%
  summarise_all(mean)

## PCA on PC LSPs
# Don't re-scale the PCs
pc_traits <- grep("^PC", colnames(pheno), value=TRUE)
pc_pca <- pheno %>%
  ungroup() %>%
  select_(.dots=pc_traits) %>%
  drop_na() %>%
  prcomp() %$% x
info <- pheno %>%
  select_(.dots=pc_traits) %>%
  drop_na() %>%
  select_(.dots=info_cols)
pc_pca <- data.frame(pc_pca, info)%>%
  group_by_(.dots=info_cols, include="RANGE") %>%
  summarise_all(mean)

heritability <- data.frame(Trait=factor(),
                           Method=factor(),
                           H2=numeric())

# Get heritability of Manual PCs
for(trait in grep("PC", colnames(manual_pca), value=TRUE)){
  print(paste0("Calculating heritability for: ", trait))
  this_model <- make_phenotypic_model(manual_pca, trait, plots=FALSE)
  this_H2 <- estimate_heritability(this_model$blup, manual_pca)
  
  this_result <- data.frame(Trait=trait, Method="Manual", H2=this_H2)

  heritability <- rbind(heritability, this_result)  
}

# Get heritability of Encoder PCs
for(trait in grep("PC", colnames(enc_pca), value=TRUE)){
  print(paste0("Calculating heritability for: ", trait))
  this_model <- make_phenotypic_model(enc_pca, trait, plots=FALSE)
  this_H2 <- estimate_heritability(this_model$blup, enc_pca)
  
  this_result <- data.frame(Trait=trait, Method="ENC", H2=this_H2)
  
  heritability <- rbind(heritability, this_result)  
}

# Get heritability of Manual PCs
for(trait in grep("PC", colnames(pc_pca), value=TRUE)){
  print(paste0("Calculating heritability for: ", trait))
  this_model <- make_phenotypic_model(pc_pca, trait, plots=FALSE)
  this_H2 <- estimate_heritability(this_model$blup, pc_pca)
  
  this_result <- data.frame(Trait=trait, Method="PC", H2=this_H2)
  
  heritability <- rbind(heritability, this_result)  
}

# Make figure
heritability <- heritability %>%
  group_by(Method) %>%
  mutate(Cumulative_H2 = cumsum(H2))
ggplot(heritability, aes(Trait, Cumulative_H2, color=Method, group=Method)) +
  geom_line() +
  geom_point() +
  theme_classic() +
  expand_limits(y=0)

ggplot(heritability %>% filter(Method=="Manual"), aes(Trait, Cumulative_H2, color=Method, group=Method)) +
  geom_line() +
  geom_point() +
  ylim(0, 5) +
  theme_classic()
