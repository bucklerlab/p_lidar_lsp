# p_lidar_lsp

Code repository for _In-field whole plant maize architecture characterized by Latent Space Phenotyping_.

Primary contact: Joseph Gage, jlg374@cornell.edu

Secondary contacts:

 * Sara Miller, sjm336@cornell.edu
 * Edward S. Buckler, esb33@cornell.edu
 
 
##  data:
 
 Contains small, original files necessary for processing lidar data, such as raw manually-measured phenotypes, list of plots that were removed manually from LiDar analysis, and metadata used when splitting data files down to single-plot level.
 
##  src:
 Contains source code for processing lidar data.  To replicate analyses done in the paper, run the scripts prefaced with numbers 1-6 in order.  File paths may need to be changed inside scripts.
 
 * **1_LiDar_import_clean_makeDensities.R**:
 * **2_process_raw_point_clouds.R**:
 * **3_remoteAutoencoder.sh**:
 * **4_make_clean_dataset.R**:
 * **5_PLSR.R**:
 * **6_model_phenotypes.R**:
 
 Autoencoder model architecture is contained in the file _lidar_autoencoder_forRemote.py_.
 